-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2017 at 05:10 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `latihanci`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_uploadimage`
--

CREATE TABLE IF NOT EXISTS `tb_uploadimage` (
`id` int(3) NOT NULL,
  `nm_gbr` varchar(35) NOT NULL,
  `tipe_gbr` varchar(10) NOT NULL,
  `ket_gbr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `alamat`, `pekerjaan`, `username`, `password`) VALUES
(1, 'James', '', 'Bandung', 'Mahasiswa', '', ''),
(2, 'Yoslie', 'yoslie.tan@ymail.com', 'Bandung', 'Coder', 'yoslie', 'd02897cbfce3da7792490bb6c8a2a88a'),
(3, 'Mangzzz', 'mamang@ymail.com', 'Bandung', 'Mahasiswa', 'mamang', '3bd3feb3f927d7c1dace62e7997bcd94'),
(4, 'jupe', 'jupe@ymail.com', 'jakarta', 'artis', 'jupe', '624dc88a646d7d0acc101405792be4f2'),
(5, 'supri', 'supri@yahoo.com', 'jakarta', 'supir', 'supri', 'd79444495ba8886c397b418227564d3f'),
(6, 'cucung', 'cucung@gmail.com', 'jakarta', 'satpam', 'cucung', 'b08bb8ac338db76d69fe4203766b8f00'),
(8, 'rio', 'riorio@gmail.com', 'Bandung', 'Mahasiswa', 'rio', 'd5ed38fdbf28bc4e58be142cf5a17cf5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_uploadimage`
--
ALTER TABLE `tb_uploadimage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_uploadimage`
--
ALTER TABLE `tb_uploadimage`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
