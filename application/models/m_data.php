<?php

class M_data extends CI_Model {

    var $tabel = 'user';

//    function ambil_data() {
//        return $this->db->get('user');
//    }

    function ambil_data($number, $offset) {
        return $query = $this->db->get('user', $number, $offset)->result();
    }

    function jumlah_data() {
        return $this->db->get('user')->num_rows();
    }

    function edit_data($where, $table) {
        return $this->db->get_where($table, $where);
    }

    function update_data($where, $data, $table) {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function hapus_data($where, $table) {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_allbarang($batas = null, $offset = null, $key = null) {
        $this->db->from($this->tabel);
        if ($batas != null) {
            $this->db->limit($batas, $offset);
        }
        if ($key != null) {
            $this->db->or_like($key);
        }
        $query = $this->db->get();

        //cek apakah ada barang
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function count_barang() {
        $query = $this->db->get($this->tabel)->num_rows();
        return $query;
    }

    function count_barang_search($orlike) {
        $this->db->or_like($orlike);
        $query = $this->db->get($this->tabel);

        return $query->num_rows();
    }

}
