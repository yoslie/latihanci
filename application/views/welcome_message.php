<!DOCTYPE html>
<html lang="en">
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Intro Header -->
        <header class="intro">
            <div class="intro-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h1 class="brand-heading">CI</h1>
                            <p class="intro-text">Silakan klik link
                                <?php echo anchor('login', 'Masuk'); ?>
                                untuk masuk ke dalam sistem.</p>
                            <a href="#about" class="btn btn-circle page-scroll">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

    </body>

</html>
