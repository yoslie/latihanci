<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">Login</h1>
                        <?php
                        // Cetak jika ada notifikasi
                        if ($this->session->flashdata('sukses')) {
                            echo '<p class="warning" style="margin: 10px 20px;">' . $this->session->flashdata('sukses') . '</p>';
                        }
                        ?>
                        <!--<form class="form-horizontal">-->
                            <?php echo form_open('login'); ?>
                            <div class="form-group">
                                <p class="control-label col-sm-2">Username:</p>
                                <p class="col-sm-10">
                                    <input class="w3-input w3-border w3-grey" type="text" name="username" placeholder="Enter username" value="<?php echo set_value('username'); ?>"/>
                                    <?php echo form_error('username'); ?>
                                </p>
                            </div>
                            <div class="form-group">
                                <p class="control-label col-sm-2">Password:</p>
                                <p class="col-sm-10">
                                    <input class="w3-input w3-border w3-grey" type="password" name="password" placeholder="Enter password" value="<?php echo set_value('password'); ?>"/>
                                    <?php echo form_error('password'); ?> 
                                </p>
                            </div>
                            <p>
                                <button class="w3-btn w3-light-grey w3-border w3-border-white w3-round" type="submit" name="btnSubmit">Login</button>
                            </p>

                            <?php echo form_close(); ?>
                        <!--</form>-->
                        <p>
                            Kembali ke beranda, Silakan klik <?php echo anchor(site_url() . '/welcome', 'di sini..'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </header>
</body>
</html>
