<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>  
<head>
    <meta charset="UTF-8">
    <title>
        Notifikasi | Code Igniter
    </title>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <h3><?php echo $message; ?></h3>
                <p><?php echo anchor('dashboard', 'Kembali ke dashboard'); ?></p>
            </div>
        </div>
    </div>
</body>
</html>