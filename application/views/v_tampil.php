<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <p><?php echo $this->session->flashdata('pesan') ?> </p>
                <div class="col-md-12">
                    <h1 class="brand-heading">Data Member</h1>
                    <!--<div class="col-md-5 pull-left">-->
                        <a href="http://latihanci.local/index.php/register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
                    <!--</div>-->
                    <div class="col-md-5 pull-right">
                        <form action="<?php echo base_url(); ?>index.php/crud/cari" method="get">
                            <div class="input-group">

                                <input type="text" name="key" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">cari</button>
                                </span>
                            </div><!-- /input-group -->

                        </form>
                    </div>
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Pekerjaan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (empty($user)) { ?>
                                <tr>
                                    <td colspan="6">Data tidak ditemukan</td>
                                </tr>
                                <?php
                            } else {

                                if (!$jlhpage) {
                                    $no = 0;
                                } else {
                                    $no = $jlhpage;
                                }

                                foreach ($user as $u) {
                                    $no++;
                                    ?>
                                    <tr>
                                        <td><?php echo $no ?></td>
                                        <td><?php echo $u->nama ?></td>
                                        <td><?php echo $u->alamat ?></td>
                                        <td><?php echo $u->pekerjaan ?></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>index.php/crud/edit/<?php echo $u->id ?>" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
                                            <a href="<?php echo base_url() ?>index.php/crud/hapus/<?php echo $u->id ?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda Yakin menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!--                <ul class="pagination">
                                    <li>-->
                <div class="panel-footer">
                    <?php echo $paging ?>
                </div>  <!-- /panel-footer-->
                <!--                    </li>
                                </ul>-->
            </div>
        </div>
    </div>
</body>