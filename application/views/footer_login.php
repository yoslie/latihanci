<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets_new/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets_new/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url(); ?>assets_new/js/plugins/morris/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_new/js/plugins/morris/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_new/js/plugins/morris/morris-data.js"></script>
</body>