<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->simple_login->cek_login();
        $this->load->helper('html');
    }

    public function index() {
        $this->load->view('header_awal');
        $this->load->view('welcome_message');
        $this->load->view('footer');
    }

}
