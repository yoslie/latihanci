<?php

class Upload extends CI_Controller {

    var $limit = 10;
    var $offset = 10;

    function __construct() {
        parent::__construct();
        $this->load->model('mupload'); //load model mupload yang berada di folder model
        $this->load->helper(array('url'));
//        $this->load->helper(array('form', 'url'));
    }

//    public function index() {
//        $this->load->view('v_upload', array('error' => ' '));
//    }
//
//    public function aksi_upload() {
//        $config['upload_path'] = './gambar/';
//        $config['allowed_types'] = 'gif|jpg|png';
//        $config['max_size'] = 100;
//        $config['max_width'] = 1024;
//        $config['max_height'] = 768;
//
//        $this->load->library('upload', $config);
//
//        if (!$this->upload->do_upload('berkas')) {
//            $error = array('error' => $this->upload->display_errors());
//            $this->load->view('v_upload', $error);
//        } else {
//            $data = array('upload_data' => $this->upload->data());
//            $this->load->view('v_upload_sukses', $data);
//        }
//    }
    public function index($page = NULL, $offset = '', $key = NULL) {
        $data['titel'] = 'Upload CodeIgniter'; //varibel title

        $data['query'] = $this->mupload->get_allimage(); //query dari model

        $this->load->view('vupload', $data); //tampilan awal ketika controller upload di akses
    }

    public function add() {

        $data['titel'] = 'Form Upload CodeIgniter'; //varibel title
        //view yang tampil jika fungsi add diakses pada url
        $this->load->view('fupload', $data);
    }

    public function insert() {
        $this->load->library('upload');
        $nmfile = "file_" . time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './assets/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width'] = '1288'; //lebar maksimum 1288 px
        $config['max_height'] = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);

        if ($_FILES['filefoto']['name']) {
            if ($this->upload->do_upload('filefoto')) {
                $gbr = $this->upload->data();
                $data = array(
                    'nm_gbr' => $gbr['file_name'],
                    'tipe_gbr' => $gbr['file_type'],
                    'ket_gbr' => $this->input->post('textket')
                );

                $this->mupload->get_insert($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Upload gambar berhasil !!</div></div>");
                redirect('upload'); //jika berhasil maka akan ditampilkan view vupload
            } else {
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Gagal upload gambar !!</div></div>");
                redirect('upload/add'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }

}
