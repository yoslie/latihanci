<?php

class Crud extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->simple_login->cek_login();
        $this->load->model('m_data');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->database();
    }

    function index() {
        $this->load->view('header_login');
        $page = $this->input->get('per_page');
        $batas = 5; //jlh data yang ditampilkan per halaman
        if (!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
            $offset = 0;
        else:
            $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url() . 'index.php/crud?';   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->m_data->count_barang(); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="current"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['paging'] = $this->pagination->create_links();
        $data['jlhpage'] = $page;

        $data['title'] = 'CRUD CodeIgniter Studi Kasus Barang'; //judul title
        $data['user'] = $this->m_data->get_allbarang($batas, $offset); //query model semua barang

        $this->load->view('v_tampil', $data);
        $this->load->view('footer_login');
    }

    public function cari() {
        $this->load->view('header_login');
        $key = $this->input->get('key'); //method get key
        $page = $this->input->get('per_page');  //method get per_page

        $search = array(
            'nama' => $key
        ); //array pencarian yang akan dibawa ke model

        $batas = 5; //jlh data yang ditampilkan per halaman
        if (!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
            $offset = 0;
        else:
            $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url() . 'crud/?key=' . $key;   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->m_data->count_barang_search($search); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Prev';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="current"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['paging'] = $this->pagination->create_links();
        $data['jlhpage'] = $page;

        $data['title'] = 'CRUD CodeIgniter Studi Pencarian Kasus User'; //judul title
        $data['user'] = $this->m_data->get_allbarang($batas, $offset, $search); //query model semua barang

        $this->load->view('v_tampil', $data);
        $this->load->view('footer_login');
    }

    function hapus($id) {
        $where = array('id' => $id);
        $this->m_data->hapus_data($where, 'user');
        redirect('crud/index');
    }

    function edit($id) {
        $this->load->view('header_login');
        $where = array('id' => $id);
        $data['user'] = $this->m_data->edit_data($where, 'user')->result();
        $this->load->view('account/v_edit', $data);
        $this->load->view('footer_login');
    }

    function update() {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $pekerjaan = $this->input->post('pekerjaan');

        $data = array(
            'nama' => $nama,
            'alamat' => $alamat,
            'pekerjaan' => $pekerjaan
        );

        $where = array(
            'id' => $id
        );

        $this->m_data->update_data($where, $data, 'user');
        redirect('crud');
    }

}
